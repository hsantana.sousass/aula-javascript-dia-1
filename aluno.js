// Gera um array com a quatidade de notas que o usuário desejar
function geradorNotasRandom(quatidadeNota){
    let boletim = [];
    for (let i = 0; i < quatidadeNota; i++){
        boletim.push(10 * Math.random());
    }
    return boletim;
}

function mediaAritmetica(arrayNotas){
    let soma = 0;
    let tamanho = 0;

    for (nota of arrayNotas){
        soma += nota;
        tamanho++;
    }

    return soma/tamanho;
}

function situacaoAluno(media){
    return media >= 6? "Aprovado":"Reprovado";
}

function mostrarNotas(arrayNotas){
    let i = 1;
    for (nota of arrayNotas){
        console.log(`Nota ${i} = ${nota}`);
        i++;
    }
    console.log(`Média = ${mediaAritmetica(arrayNotas)}`)
}

let vetNotas = geradorNotasRandom(3);

mostrarNotas(vetNotas);
console.log(situacaoAluno(mediaAritmetica(vetNotas)));