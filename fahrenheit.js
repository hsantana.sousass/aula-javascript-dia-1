// Coverte graus fahrenheit para graus celsius
function conversorDeTemperatura(temperaturaFahrenheit) {
    return 5 * ((temperaturaFahrenheit - 32)/9)
}

let tf = 150 * Math.random(); //Gera número de 0 a 150

console.log(`${tf} Graus fahrenheit -> ${conversorDeTemperatura(tf)} Graus celsius`);